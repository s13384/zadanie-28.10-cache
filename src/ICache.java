import java.util.ArrayList;


public interface ICache {

	public void clean();
	public void put(SystemEnumeration element);
	public void changeNewest();
	public ArrayList<SystemEnumeration> get(String key);
	public ICache getInstance2();
}
