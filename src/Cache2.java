import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Cache2 implements ICache{

	private static Cache2 instance;
	private ArrayList<HashMap<String, ArrayList<SystemEnumeration>>> cacheObjects = new ArrayList<HashMap<String, ArrayList<SystemEnumeration>>>();
	private int newest;
	
	private Cache2(){
		newest = 1;
		cacheObjects.add(new HashMap<String, ArrayList<SystemEnumeration>>());
		cacheObjects.add(new HashMap<String, ArrayList<SystemEnumeration>>());
		
	}
	public static Cache2 getInstance(){
		if (instance==null) 
			instance = new Cache2();
		return instance;
	}
	public void clean(){
		cacheObjects.get(1-newest).clear();
	}
	public void put(SystemEnumeration element){
		if (! (cacheObjects.get(1-newest).containsKey(element.getEnumerationName()))){
			cacheObjects.get(1-newest).put(element.getEnumerationName(), new ArrayList<SystemEnumeration>());
		}
		cacheObjects.get(1-newest).get(element.getEnumerationName()).add(element);
	}
	public void changeNewest(){
		newest = 1 - newest;
	}	
	public ArrayList<SystemEnumeration> get(String key){
		return cacheObjects.get(newest).get(key);
	}
	@Override
	public ICache getInstance2() {
		return instance;
	}
}