package demo;

public class Singleton {

	private static Singleton instance;
	private String name;
	private Singleton(){
		
	}
	public static Singleton getInstance()	{
		if (instance==null)					// jak zrobic zeby 2 watki nie zrobily w tym samym czasie 2 instancji
			instance = new Singleton();
		return instance;
	}
	public void DoWork(){}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}