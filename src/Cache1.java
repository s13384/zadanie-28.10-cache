import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Cache1  implements ICache{

	private static Cache1 instance;
	private HashMap<String, ArrayList<SystemEnumeration>> cacheObjects = new HashMap<String, ArrayList<SystemEnumeration>>();
	
	private Cache1(){
		
	}
	public static Cache1 getInstance(){
		if (instance==null) 
			instance = new Cache1();
		return instance;
	}
	public void clean(){
		cacheObjects.clear();
	}
	public void put(SystemEnumeration element){
		if (! (cacheObjects.containsKey(element.getEnumerationName()))){
			cacheObjects.put(element.getEnumerationName(), new ArrayList<SystemEnumeration>());
		}
		cacheObjects
		.get(element.getEnumerationName())
		.add(element);
	}
	public void changeNewest(){
		
	}	
	public ArrayList<SystemEnumeration> get(String key){
		return cacheObjects.get(key);
	}
	@Override
	public ICache getInstance2(){
		return instance;
	}
}