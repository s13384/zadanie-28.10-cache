import java.util.ArrayList;

import demo.IntPrinter;
import demo.Singleton;


public class Main {

	public static void main(String[] args) {
	
		/*
		Singleton s = Singleton.getInstance();
		
		s.setName("Singletonnnn");
		
		Singleton s2 = Singleton.getInstance();
		
		System.out.println(s2.getName());

		IntPrinter p1 = new IntPrinter();
		IntPrinter p2 = new IntPrinter();
		IntPrinter p3 = new IntPrinter();
		IntPrinter p4 = new IntPrinter();
		IntPrinter p5 = new IntPrinter();
		
		Thread t1 = new Thread(p1);
		Thread t2 = new Thread(p2);
		Thread t3 = new Thread(p3);
		Thread t4 = new Thread(p4);
		Thread t5 = new Thread(p5);
		t1.start();
		t2.start();
		t3.start();
		t5.start();
		t4.start();
		*/
		ICache cache = Cache2.getInstance();
		ArrayList<SystemEnumeration> latestData = new ArrayList<SystemEnumeration>();
		latestData.add(new SystemEnumeration(1101, 1, "POM", "woj pomorskie", "REGION"));
		latestData.add(new SystemEnumeration(1102, 2, "KPOM", "woj kujawsko-pomorskie", "REGION"));
		latestData.add(new SystemEnumeration(2801, 1, "KOM", "tel komorkowy", "PHONE-TYPE"));
		latestData.add(new SystemEnumeration(2801, 2, "ST", "tel stacjonarny", "PHONE-TYPE"));
		latestData.add(new SystemEnumeration(3001, 1, "DSA", "ddos strong attack", "DDOS"));
		latestData.add(new SystemEnumeration(3002, 2, "DA", "ddos attack", "DDOS"));
		Updater updater = new Updater();
		updater.setLifespan(10000);
		updater.setCacheInstance(cache.getInstance2());
		updater.setLatestData(latestData);
		updater.start();
		//cache.put(new SystemEnumeration(1101, 1, "POM", "woj pomorskie", "Region"));
		//cache.put(new SystemEnumeration(1102, 2, "KPOM", "woj kujawsko-pomorskie", "Region"));
		//cache.put(new SystemEnumeration(2801, 1, "KOM", "tel komorkowy", "PHONE-TYPE"));
		//cache.put(new SystemEnumeration(2801, 2, "ST", "tel stacjonarny", "PHONE-TYPE"));
		System.out.println("+++++++");
		CacheTester test = new CacheTester();
		test.setTestTime(100);
		test.start(cache.getInstance2());
		
	}

}